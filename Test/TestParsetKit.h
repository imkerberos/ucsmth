//
//  TestParsetKit.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestParsetKit : NSObject
+ (void) testParseKitBoardPages;
+ (void) testParseKitBoardPages2;
+ (void) test4;
@end

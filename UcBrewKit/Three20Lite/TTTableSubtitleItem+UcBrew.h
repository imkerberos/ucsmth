//
//  TTTableSubtitleItem+UcBrew.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTTableSubtitleItem.h"

@interface TTTableSubtitleItem (UcBrew)

+ (id)itemWithText: (NSString *)text
          subtitle: (NSString *)subtitle
         accessory: (UITableViewCellAccessoryType) accessoryType;

@end
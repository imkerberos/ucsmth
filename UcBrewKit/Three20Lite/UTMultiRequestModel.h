//
//  UTMultiRequestModel.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-19.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTModel.h"
#import "TTURLRequestDelegate.h"

@interface UTMultiRequestModel : TTModel<TTURLRequestDelegate>
@property (nonatomic, assign) BOOL isCancelAll;
@end

//
//  UcBrew.m
//  UcBrewKit
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrew.h"

////////////////////////////////////////////////////////////////////////////////
@implementation UcBrew
+ (NSString *) NSSearchPathForDirectoriesInDomains:(NSSearchPathDirectory)searchPath
{
    NSArray *a = NSSearchPathForDirectoriesInDomains(searchPath, NSUserDomainMask, YES);
    if (a.count) {
        return [a objectAtIndex:0];
    } else {
        return nil;
    }
}

+ (NSString*) userPath
{
    return [self NSSearchPathForDirectoriesInDomains:NSUserDirectory];
}

+ (NSString*) documentPath
{
    return [self NSSearchPathForDirectoriesInDomains:NSDocumentDirectory];
}

+ (NSString*) libraryPath
{
    return [self NSSearchPathForDirectoriesInDomains:NSLibraryDirectory];
}

+ (NSString*) cachesPath
{
    return [self NSSearchPathForDirectoriesInDomains:NSCachesDirectory];
}

+ (NSString*) downloadsPath
{
    return [self NSSearchPathForDirectoriesInDomains:NSDownloadsDirectory];
}

+ (NSString*) appPath
{
    return [self NSSearchPathForDirectoriesInDomains:NSApplicationDirectory];
}

+ (NSString*) bundlePath
{
    return [[NSBundle mainBundle] resourcePath];
}
@end
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
BOOL UcBrew_IsNotEmptyString(id object)
{
    return [object isKindOfClass: [NSString class]] && ([object length] > 0);
}
////////////////////////////////////////////////////////////////////////////////

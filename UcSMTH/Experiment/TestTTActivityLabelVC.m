//
//  TestTTActivityLabelVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-25.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "TestTTActivityLabelVC.h"

@interface TestTTActivityLabelVC ()

@end

@implementation TestTTActivityLabelVC

+ (id) viewController
{
    TestTTActivityLabelVC* vc = [[TestTTActivityLabelVC alloc] init];
    return [vc autorelease];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    TTActivityLabel* label = [[[TTActivityLabel alloc] initWithStyle:TTActivityLabelStyleWhiteBox]
                              autorelease];
    UCBREW_LOG_OBJECT(NSStringFromCGRect(label.frame));
    label.text = @"Test label";
    //label.backgroundColor = self.htmlView.backgroundColor;
    label.backgroundColor = [UIColor blackColor];
    [self.view addSubview: label];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

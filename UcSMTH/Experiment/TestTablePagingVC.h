//
//  TestTablePagingVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-24.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USPageVC.h"
#import "TTTableViewController.h"

@interface TestTablePagingVC : TTTableViewController <USPageVCDelegate>
+ (id) viewController;
@end

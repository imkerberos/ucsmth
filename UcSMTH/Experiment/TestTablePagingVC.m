//
//  TestTablePagingVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-24.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "TestTablePagingVC.h"

@interface TestTablePagingVC ()
@property (nonatomic, assign) BOOL isPaged;
@property (nonatomic, retain) USPageVC* pagingVC;
@property (nonatomic, retain) UIView* maskView;
@end

@implementation TestTablePagingVC
@synthesize isPaged;
@synthesize pagingVC;
@synthesize maskView;

+ (id) viewController
{
    TestTablePagingVC* vc = [[TestTablePagingVC alloc] initWithStyle: UITableViewStylePlain];
    return [vc autorelease];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE(pagingVC);
    UCBREW_RELEASE(maskView);
    [super dealloc];
}

- (void) togglePaging
{
    CGPoint point = CGPointMake(0, 208);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    if (isPaged) {
        [self.pagingVC viewWillDisappear: NO];
        self.maskView.center = CGPointMake(160.0f, 208.0f);
        self.tableView.center = CGPointMake(160.0f, 208.0f);
        self.tableView.opaque = YES;
        isPaged = NO;
    } else {
        [self.pagingVC viewWillAppear: NO];
        isPaged = YES;
        self.tableView.opaque = YES;
        self.maskView.center = point;
        self.tableView.center = point;
    }
    [UIView commitAnimations];
    if (isPaged) {
        [self.pagingVC viewDidDisappear:NO];
    } else {
        [self.pagingVC viewDidAppear: NO];
    }
}

- (void) handleRecongnizerFrom: (UISwipeGestureRecognizer*) recongnizer
{
    UCBREW_LOG(@"paged: %d, direction: %d", self.isPaged, recongnizer.direction);
    if (recongnizer.direction == UISwipeGestureRecognizerDirectionRight && self.isPaged) {
        [self togglePaging];
    } else if (recongnizer.direction == UISwipeGestureRecognizerDirectionLeft && !self.isPaged) {
        [self togglePaging];
    }
}

- (void) setupRecongnizer
{
    UISwipeGestureRecognizer* recongnizer = nil;
    recongnizer = [[UISwipeGestureRecognizer alloc] initWithTarget: self action: @selector(handleRecongnizerFrom:)];
    recongnizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer: recongnizer];
    
    recongnizer = [[UISwipeGestureRecognizer alloc] initWithTarget: self action: @selector(handleRecongnizerFrom:)];
    recongnizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer: recongnizer];
}

- (void) setupPaging
{
    self.view.backgroundColor = [UIColor whiteColor];
    CGRect r = self.tableView.frame;
    self.maskView = [[[UIView alloc] initWithFrame: r] autorelease];
    self.maskView.layer.masksToBounds = NO;
    self.maskView.opaque = NO;
    self.maskView.layer.shadowOffset = CGSizeMake(5, 3);
    self.maskView.layer.shadowOpacity = 0.6f;
    self.maskView.layer.shadowColor = [UIColor blackColor].CGColor; 
    self.maskView.backgroundColor = [UIColor whiteColor];
    self.pagingVC = [USPageVC viewControllerWithIndexOfPage: 1 totalOfPage: 18 delegate: self];
    [self.view insertSubview: self.maskView belowSubview: self.tableView];
    [self.view insertSubview: self.pagingVC.view belowSubview:self.maskView];
    CGPoint point = self.pagingVC.view.center;
    point.y -= 20;
    self.pagingVC.view.center = point;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupRecongnizer];
    [self setupPaging];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void) createModel
{
    self.dataSource = [TTListDataSource dataSourceWithObjects:
            [TTTableTextItem itemWithText: @"Item1"],
            [TTTableTextItem itemWithText: @"Item2"],
            [TTTableTextItem itemWithText: @"Item3"],
            [TTTableTextItem itemWithText: @"Item4"],
            [TTTableTextItem itemWithText: @"Item5"],
            [TTTableTextItem itemWithText: @"Item6"],
            nil];
}

- (void) controller:(USPageVC *)controller didFinishPaging:(NSUInteger)page
{
    UCBREW_CALL_METHOD;
    [self togglePaging];
    UCBREW_LOG(@"paging to: %d", page);
}
@end
//
//  USExperimentVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTTableViewController.h"

@interface USExperimentVC : TTTableViewController
+ (id) viewController;
@end

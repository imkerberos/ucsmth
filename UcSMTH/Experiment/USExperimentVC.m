//
//  USExperimentVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"

#import "USExperimentVC.h"
#import "USRecommendVC.h"
#import "USArticleWebVC.h"
#import "USNavigationController.h"
#import "TestStyledTextWithURLImageVC.h"
#import "TestXBPageCurlVC.h"
#import "TestTablePagingVC.h"
#import "TestTTActivityLabelVC.h"
#import "USForumLVC.h"
#import "USPhotoViewVC.h"
#import "USLoginVC.h"
#import "NFMessageComposerVC.h"
@interface USExperimentVC () <NFMessageComposerVCDelegate>
@end

@implementation USExperimentVC


+ (id) viewController
{
    USExperimentVC* vc = [[USExperimentVC alloc] initWithStyle: UITableViewStylePlain];
    return [vc autorelease];
}

- (id) initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    
    return self;
}

- (void) dealloc
{
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) createModel
{
    self.dataSource = [TTListDataSource dataSourceWithObjects:
                       [TTTableTextItem itemWithText: @"New Composer"
                                        accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText: @"CMHTMLView Test With URL"
                                           accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText:@"TTStyledText With URL Image"
                                            accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText:@"XBpageCurl test for TTTableViewControlelr"
                                            accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText:@"MWFTable for favorite"
                                            accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText:@"paging view"
                                            accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText:@"Swipe Recongnizer for TTTableViewController"
                                            accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText:@"TTActivityLabel Test"
                                            accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText:@"Test SigUp use MwfTableViewController"
                                            accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText:@"PhotoViewer"
                                            accessory: UITableViewCellAccessoryDisclosureIndicator],
                       nil];
}

- (void) didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        NFMessageComposerVC* vc = [[NFMessageComposerVC alloc] initWithBoardId: @"Apple"
                                                                     articleId: nil
                                                                            to: nil
                                                                       subject: nil
                                                                          body: nil
                                                                      delegate: self];
        [self.navigationController pushViewController: vc
                                             animated: YES];
        [vc release];
    } else if (indexPath.row == 1) {
        USArticleWebVC* vc = [[USArticleWebVC alloc] init];
        [self.navigationController pushViewController: vc animated:YES];
        [vc release];
    } else if ( indexPath.row == 2) {
        TestStyledTextWithURLImageVC* vc = nil;
        vc = [[TestStyledTextWithURLImageVC alloc] init];
        [self.navigationController pushViewController: vc animated: YES];
        [vc release];
    } else if ( indexPath.row == 3) {
        TestXBPageCurlVC* vc = [TestXBPageCurlVC viewController];
        [self.navigationController pushViewController: vc animated: YES];
    } else if ( indexPath.row == 4 ) {
        USForumLVC* vc = [USForumLVC viewController];
        [self.navigationController pushViewController: vc animated: YES];
    } else if ( indexPath.row == 5) {
        USPageVC* vc = [USPageVC viewControllerWithIndexOfPage:1 totalOfPage:18 delegate:nil];
        [self.navigationController pushViewController: vc animated: YES];
    } else if ( indexPath.row == 6 ) {
        TestTablePagingVC* vc = [TestTablePagingVC viewController];
        [self.navigationController pushViewController: vc animated: YES];
    } else if (indexPath.row == 7) {
        TestTTActivityLabelVC* vc = [TestTTActivityLabelVC viewController];
        [self.navigationController pushViewController: vc animated: YES];
        
    } else if (indexPath.row == 8) {
        USLoginVC* vc = [[[USLoginVC alloc] initWithStyle: UITableViewStyleGrouped] autorelease];
        [self.navigationController presentModalViewController: vc animated: YES];
    } else if (indexPath.row == 9) {
        USPhotoViewVC* vc = [[[USPhotoViewVC alloc] init] autorelease];
        [self.navigationController presentModalViewController: vc animated: YES];
    }
}

- (void) messageComposerDidCancel: (NFMessageComposerVC*) composerVC
{
    [self.navigationController popToViewController: self animated: YES];
}

- (void) messageComposerDidFinish: (NFMessageComposerVC*) composerVC
{
    [self.navigationController popToViewController: self animated: YES];
}
@end
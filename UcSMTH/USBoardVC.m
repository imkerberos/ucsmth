//
//  USBoardVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "UcSMTHEngine.h"
#import "USNavigationController.h"
#import "USBoardModel.h"
#import "USArticleWebVC.h"
#import "USBoardDS.h"
#import "USArticleComposer.h"
#import "USBoardVC.h"
#import "USViewModeSelectVC.h"
#import "USViewModeVC.h"
#import "USTableMoreButton.h"

@interface USBoardVC () <USViewModeVCDelegate, NFMessageComposerVCDelegate>
@property (nonatomic, copy) NSString* boardId;
@property (nonatomic, assign) BOOL isPaged;
@property (nonatomic, assign) NSUInteger startIndexOfPage;
@property (nonatomic, assign) NSUInteger viewMode;
@end

@implementation USBoardVC
@synthesize boardId;
@synthesize isPaged;
@synthesize startIndexOfPage;
//@synthesize pagingVC;
@synthesize viewMode;

+ (id) viewControllerWithBoardId:(NSString *)boardId
{
    USBoardVC* vc = [[USBoardVC alloc] initWithBoardId: boardId];
    return [vc autorelease];
}

- (id) initWithBoardId: (NSString*) aBoardId
{
    self = [super initWithStyle: UITableViewStylePlain];
    if (self) {
        self.boardId = aBoardId;
        self.startIndexOfPage = 1;
        UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
        self.viewMode = [[engine userDefaults] integerForKey: @"viewmode"];
    }
    
    return self;
}

- (NSString*) title
{
    return [[UcSMTHEngine sharedEngine] boardNameById: self.boardId];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.boardId = nil;
        self.startIndexOfPage = 1;
        UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
        self.viewMode = [[engine userDefaults] integerForKey: @"viewmode"];
    }
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE(boardId);
    [super dealloc];
}

- (void) handleComposeAction
{
    USArticleComposer* vc = [USArticleComposer composerWithMode: USArticleComposerModeNew];
    vc.boardId = self.boardId;
    vc.delegate = self;
    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc] autorelease];
    [self presentModalViewController: nav animated: YES];
}

- (void) messageComposerDidFinish: (NFMessageComposerVC*) composerVC
{
    [self dismissModalViewControllerAnimated: YES];
    if (self.startIndexOfPage == 1) {
        UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];

        if ([[engine userDefaults] boolForKey: @"postrefresh"]) {
            [self invalidateModel];
            [self invalidateView];
        }
    }
}

- (void) messageComposerDidCancel: (NFMessageComposerVC*) composerVC
{
    [self dismissModalViewControllerAnimated: YES];
}

- (void) setupNavigationBarItem
{
    UIBarButtonItem* compose_item = nil;
    compose_item = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemCompose
                                                                 target: self
                                                                 action: @selector(handleComposeAction)]
                    autorelease];
    self.navigationItem.rightBarButtonItem = compose_item;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.variableHeightRows = YES;
    [self setupNavigationBarItem];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) createModel
{
    self.dataSource = [USBoardDS dataSourceWithBoardId: self.boardId startIndexOfPage: self.startIndexOfPage viewMode: self.viewMode];
}

- (id) createDelegate
{
    TTTableViewDragRefreshDelegate* d = nil;
    d = [[[TTTableViewDragRefreshDelegate alloc] initWithController: self]
         autorelease];
    return d;
}

- (void) didLoadModel:(BOOL)firstTime
{
    [super didLoadModel: firstTime];
    if (firstTime) {
        USBoardModel* m = UCBREW_DYNAMIC_CAST(self.model, USBoardModel);
    }
}

#pragma mark -
#pragma TTTableViewController


- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    id selected_item = nil;
    selected_item = UCBREW_DYNAMIC_CAST(object, TTTableSettingsItem);
    if ([selected_item isMemberOfClass: [TTTableSettingsItem class]]) {
        USViewModeVC* vc = [USViewModeVC viewControllerWithMode: self.viewMode
                                                                   delegate: self];
        [self.navigationController pushViewController: vc animated: YES];
        return;
    }

    UTTableLongSubtitleItem* item;
    item = nil;
    item  = UCBREW_DYNAMIC_CAST( object, UTTableLongSubtitleItem);
    if ( [item isMemberOfClass: [TTTableCaptionItem class]] && indexPath.row == 0 ) {
        /*
         * top
         */
        return;
    }
    if (item) {
        NSString* postId = (NSString*) item.userInfo;
        if (UcBrew_IsNotEmptyString(boardId) && UcBrew_IsNotEmptyString(postId)) {
            USArticleWebVC* vc = [USArticleWebVC viewControllerWithBoardId: boardId articleId: postId viewMode: self.viewMode];
            [self.navigationController pushViewController: vc animated: YES];
        }
    }
}

- (void) accessoryButtonTappedForObject: (id) object atIndexPath: (NSIndexPath*) indexPath
{
    USTableMoreButton* button = UCBREW_DYNAMIC_CAST(object, USTableMoreButton);
    if (button) {
        USBoardModel* model = (USBoardModel*)self.model;
        USPageVC* vc = [USPageVC viewControllerWithIndexOfPage: self.startIndexOfPage totalOfPage:model.totalOfPage
                                                      delegate: self];
        [self.navigationController pushViewController: vc
                                             animated: YES];
    }
}

- (void) controller:(USPageVC *)controller didFinishPaging:(NSUInteger)page
{
    UCBREW_CALL_METHOD;
    UCBREW_LOG(@"paging to: %d", page);
    /*
     * Loading new model
     */
    [self.navigationController popToViewController: self animated: YES];
    self.startIndexOfPage = page;
    [self invalidateModel];
    //[m loadWithStartIndexOfPage: page];
}

#pragma mark -

- (void) viewModeDidFinishSelect: (USViewModeSelectVC*) viewController mode: (NSInteger) mode
{
    [self.navigationController popToViewController: self animated: YES];
    if (self.viewMode != mode) {
        self.viewMode = mode;
        self.startIndexOfPage = 1;
        [self invalidateModel];
    }
}
@end
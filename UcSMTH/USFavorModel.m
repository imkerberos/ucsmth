//
//  USFavorModel.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "DDXML.h"
#import "DDXML+HTML.h"
#import "UcSMTHEngine.h"
#import "USURLHtmlResponse.h"
#import "USURLRequest.h"
#import "USBoardFeed.h"
#import "USBoardFeedParser.h"
#import "USFavorModel.h"
#import "RegexKitLite.h"

@implementation USFavorModel
{
@private
    NSString* _dirName;
    NSString* _dirId;
}

@synthesize boardFeeds;
@synthesize dirName = _dirName;
@synthesize dirId = _dirId;


- (id) init
{
    self = [super init];
    if (self) {
        //self.boardFeeds = [NSMutableArray arrayWithCapacity:10];
    }
    
    return self;
}

- (id) initWithDirName: (NSString*) dirName dirId: (NSString*) dirId
{
    self = [super init];
    if (self) {
        _dirId = [dirId copy];
        _dirName = [dirName copy];
    }

    return self;

}

- (void) dealloc
{
    UCBREW_RELEASE (boardFeeds);
    [_dirName release], _dirName = nil;
    [_dirId release], _dirId = nil;
    [super dealloc];
}

- (void) load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (!self.isLoading) {
        NSString* url = @"http://m.newsmth.net/favor";
        if (_dirId) {
            url = [NSString stringWithFormat: @"http://m.newsmth.net/favor/%@", _dirId];
        }
        USURLRequest* request = [USURLRequest requestWithURL:url delegate:self];
        USURLHtmlResponse* resp = [USURLHtmlResponse response];
        self.boardFeeds = [NSMutableArray arrayWithCapacity:10];
        request.cachePolicy = TTURLRequestCachePolicyNetwork;
        request.response = resp;
        UCBREW_LOG_OBJECT(request.headers);
        [request send];
    }
}

- (void) requestDidFinishLoad:(TTURLRequest *)request
{
    USURLHtmlResponse* resp = request.response;
    
    [self parseHtmlDocument: resp.htmlDocument];
    
    [super requestDidFinishLoad:request];
}

- (void) parseHtmlDocument: (DDXMLDocument*) htmlDocument
{
    static NSString* boardHrefRegex = @"^\\/board\\/(.+)";
    static NSString* dirHrefRegex = @"^\\/favor\\/(.+)";
    NSString* xpath;
    xpath = @"//div[@id='m_main']/ul[@class='slist sec']/li/a[@href]";
    NSArray* array = [htmlDocument nodesForXPath:xpath error:nil];
    for (DDXMLElement *e in array) {
        NSString* href = [[e attributeForName: @"href"] stringValue];
        if ([href isMatchedByRegex: boardHrefRegex]) {
            USBoardFeed* feed = [[USBoardFeedParser sharedParser] parseBoardHref: href];
            if (feed) {
                feed.boardName = [[UcSMTHEngine sharedEngine] boardNameById: feed.boardId];
                [(NSMutableArray*)self.boardFeeds addObject:feed];
            }
        } else if ([href isMatchedByRegex: dirHrefRegex]) {
            USBoardFeed* feed = [[[USBoardFeed alloc] init] autorelease];
            NSRange r =  [href rangeOfRegex: dirHrefRegex capture: 1];
            feed.boardId = [href substringWithRange: r];
            feed.boardName = [e stringValue];
            feed.isSection = YES;
            [(NSMutableArray*)self.boardFeeds addObject:feed];
            UCBREW_LOG(@"directory");
        }
    }
}
@end
//
//  USHTMLView.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-22.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "UcSMTHConfig.h"
#import "USHTMLView.h"

static NSString* uc_html_default_style_format = @""
@"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0; user-scalable=0; minimum-scale=1.0; maximum-scale=1.0\"/>"
@"<style type=\"text/css\">"
@"body {"
@"margin:0;"
@"padding:9px;"
@"font-family:\"%@\";"
@"font-size:%f;"
@"word-wrap:break-word;"
@"-webkit-text-size-adjust:none;"
@"}"
@""
@"img,video,iframe {"
@"margin:5px 0 5px 0;"
@"}"
@""
@"* {"
@"height:auto;"
@"}"
@""
@"@media (orientation:portrait) {"
@"* {"
@"max-width:%.0fpx;"
@"}"
@"}"
@""
@"@media (orientation:landscape) {"
@"* {"
@"max-width:%.0fpx;"
@"}"
@"}"
@"%@"
@"</style>"
@"";

@implementation USHTMLView
@synthesize maxPortraitWidth;
@synthesize maxLandscapeWidth;
@synthesize fontFamily;
@synthesize fontSize;
@synthesize addtionalStyle;

+ (void) hackRemoveBackgroundFromWebView: (UIWebView*) webView
{
    for (UIView* subView in [webView subviews]) {
        if ([subView isKindOfClass:[UIScrollView class]]) {
            for (UIView* shadowView in [subView subviews]) {
                if ([shadowView isKindOfClass:[UIImageView class]]) {
                    [shadowView setHidden:YES];
                }
            }
        }
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor whiteColor];
        self.opaque = NO;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.scalesPageToFit = NO;
        self.allowsInlineMediaPlayback = NO;
        self.mediaPlaybackRequiresUserAction = NO;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            self.maxPortraitWidth = 320;
            self.maxLandscapeWidth = 480;
        } else {
            self.maxPortraitWidth = 768;
            self.maxLandscapeWidth = 1024;
        }
        self.fontFamily = @"Helvetica";
        self.fontSize = 20.0f;
        self.addtionalStyle = @"";
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) dealloc
{
    UCBREW_CALL_METHOD;
    UCBREW_RELEASE(addtionalStyle);
    UCBREW_RELEASE(fontFamily);
    self.delegate = nil;
    [super dealloc];
}

- (UIScrollView*) scrollView
{
#if 0
    for (id subview in self.webView.subviews) {
        if ([[subview class] isSubclassOfClass: [UIScrollView class]]) {
            return subview;
        }
    }    
#endif
    return nil;
}
- (void) loadHTMLBody:(NSString *)htmlSource baseUrl:(NSString *)baseUrl
{
    
    NSURL* base_url = nil;
    if (!! [baseUrl length]) {
        base_url = [NSURL URLWithString: baseUrl];
    }
#if 0
    NSString* default_style = [NSString stringWithFormat: uc_html_default_style_format,
                               fontFamily, self.fontSize,
                               self.maxPortraitWidth, self.maxLandscapeWidth,
                               self.addtionalStyle];
    NSString* real_html = [NSString stringWithFormat:@"<html><head>%@</head><body>%@</body></html>", default_style, htmlSource];
#endif
    NSMutableString* real_html_source = [NSMutableString stringWithCapacity: htmlSource.length];
    [real_html_source appendString: @"<html><head>"];
    [real_html_source appendString:@"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0; user-scalable=0; minimum-scale=1.0; maximum-scale=1.0\"/>" @"<style type=\"text/css\">" @"body {" @"margin:0;" @"padding:9px;"];
    [real_html_source appendFormat: @"font-family:\"%@\";" @"font-size:%.0f;", self.fontFamily, self.fontSize];
    [real_html_source appendString: @"word-wrap:break-word;" @"-webkit-text-size-adjust:none;" @"}" @"" @"img,video,iframe {" @"margin:5px 0 5px 0;" @"}" @"" @"* {" @"height:auto;" @"}" @"" @"@media (orientation:portrait) {" @"* {"];
    [real_html_source appendFormat: @"max-width:%.0fpx;", self.maxPortraitWidth];
    [real_html_source appendString: @"}" @"}" @"" @"@media (orientation:landscape) {" @"* {"];
    [real_html_source appendFormat: @"max-width:%.0fpx;" @"}" @"}", self.maxLandscapeWidth];
    [real_html_source appendString: @"</style>"@""];
    [real_html_source appendFormat: @"%@", self.addtionalStyle];
    [real_html_source appendString: @"</head><body>"];
    
    [real_html_source appendString: htmlSource];
    [real_html_source appendString: @"</body></html>"];
    [real_html_source replaceOccurrencesOfString:@"</br>"
                                      withString:@""
                                         options: NSLiteralSearch
                                           range: NSMakeRange(0, real_html_source.length)
     ];
    
#ifdef UCSMTH_CONFIG_USE_FIX_WEBKIT_RELATION_IMAGE_URL
    [self loadHTMLString: real_html_source baseURL: nil];
#else
    [self loadHTMLString: real_html_source baseURL: base_url];
#endif
}

@end

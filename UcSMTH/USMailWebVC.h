//
//  USMailWebVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTModelViewController.h"

@interface USMailWebVC : TTModelViewController
+ (id) viewControllerWithBoxId: (NSString*) aBoxId mailId: (NSString*) aMailId;
@end

//
//  USMailWebVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "UcSMTHEngine.h"
#import "USPageVC.h"
#import "USHTMLView.h"
#import "USMailComposer.h"
#import "USNavigationController.h"
#import "USMailWebModel.h"
#import "USMailWebVC.h"

@interface USMailWebViewDelegate : NSObject <UIWebViewDelegate>
@property (nonatomic, assign) id <UIWebViewDelegate> delegate;
@end

@implementation USMailWebViewDelegate
@synthesize delegate;

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (self.delegate && [self.delegate respondsToSelector: @selector(webView:shouldStartLoadWithRequest:navigationType:)]) {
        return [self.delegate webView: webView shouldStartLoadWithRequest: request navigationType: navigationType];
    }
    return YES;
}
@end



@interface USMailWebVC () <UIWebViewDelegate, USPageVCDelegate, NFMessageComposerVCDelegate>
@property (nonatomic, copy) NSString* boxId;
@property (nonatomic, copy) NSString* mailId;
@property (nonatomic, retain) USHTMLView* htmlView;
@property (nonatomic, retain) UIView* overlayView;
@property (nonatomic, retain) USMailWebViewDelegate* webViewDelegate;

@property (nonatomic, retain) TTActivityLabel* loadingView;

@end

@implementation USMailWebVC
@synthesize boxId;
@synthesize mailId;
@synthesize overlayView;
@synthesize htmlView;
@synthesize loadingView;
@synthesize webViewDelegate;

+ (id) viewControllerWithBoxId:(NSString *)aBoxId mailId:(NSString *)aMailId
{
    USMailWebVC* vc = [[USMailWebVC alloc] initWithBoxId: aBoxId mailId: aMailId];
    return [vc autorelease];
}

- (id) initWithBoxId: (NSString*) aBoxId mailId: (NSString*) aMailId
{
    self = [super init];
    if (self) {
        self.boxId = aBoxId;
        self.mailId = aMailId;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupHTMLView];
    [self setupViews];
    UIBarButtonItem* reply_item = nil;
    reply_item = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemReply
                                                                target: self
                                                                action: @selector(handleComposeAction)]
                  autorelease];
    self.navigationItem.rightBarButtonItem = reply_item;

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.webViewDelegate.delegate = nil;
    self.htmlView.delegate = nil;
    UCBREW_RELEASE (htmlView);
    UCBREW_RELEASE (loadingView);
    UCBREW_RELEASE (overlayView);

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) loadView
{
    [super loadView];
    self.htmlView = [[[USHTMLView alloc] initWithFrame: CGRectZero] autorelease];
    self.webViewDelegate = [[[USMailWebViewDelegate alloc] init] autorelease];
    self.htmlView.delegate = self.webViewDelegate;
    self.webViewDelegate.delegate = self;
    [self.view addSubview: self.htmlView];
}

- (void) setupHTMLView
{
    self.htmlView.addtionalStyle = @"<style type=\"text/css\"> body{ background: #eaeaea; } .article { border: 1px solid #9d9d9d; padding: 8px; margin-top: 16px; margin-bottom: 16px; -webkit-border-radius: 8px; background: #FFFFFF; } .subject { font-size: 18px; text-align: center; } .page {font-size: 16px; text-align: center; color: #333333;} .article_header { margin-bottom: 8px; } .line { margin-left: -8px; margin-right: -8px; border-top: 1px solid #9d9d9d; } .author { color: #333333; } .author a:link { text-decoration:none; font-size: 16px; } .date { float: right; color: #999999; } .content { padding-top: 8px; }#loadmore { background: #FFFFFF; border: 1px solid #9d9d9d; padding: 8px; margin-top: 20px; font-size: 18px; text-align: center; -webkit-border-radius: 8px; }img{vertical-align:middle;}</style>";
    self.htmlView.fontSize = 16.0f;
}

- (void) handleComposeAction
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    USMailWebModel* m = (USMailWebModel*) [self model];
    NSString* mail_subject = m.mailSubject;
    NSMutableString* mail_body = nil;
    NSString* mail_author = m.mailAuthor;

    NSRange r = NSMakeRange (0, 3);
    if (mail_subject && [mail_subject length] < 3) {
        r = NSMakeRange (0, [mail_subject length]);
    }

    if ([[mail_subject substringWithRange: r] isEqualToString: @"Re:"]) {

    } else {
        mail_subject = [NSString stringWithFormat: @"Re:%@", m.mailSubject];
    }

    mail_body = [NSMutableString stringWithFormat: @"%@", [engine generateReplyMailWithContent: m.mailBody
                                                                                        author: m.mailAuthor]];
    USMailComposer* vc = [USMailComposer composerWithMode: USMailComposerModeReply];
    vc.delegate = self;
    vc.to = m.mailAuthor;
    vc.subject = mail_subject;
    vc.body = mail_body;
    vc.boxId = self.boxId;
    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc] autorelease];
    [self presentModalViewController: nav animated: YES];
}

- (void) messageComposerDidFinish: (NFMessageComposerVC*) composerVC
{
    [self dismissModalViewControllerAnimated: YES];
}

- (void) messageComposerDidCancel: (NFMessageComposerVC*) composerVC
{
    [self dismissModalViewControllerAnimated: YES];
}

- (void) setupViews
{
    CGRect r = self.view.bounds;
    r.size.height -= 0;
    self.htmlView.frame = r;
}

- (CGRect)rectForOverlayView {
    return [self.htmlView frame];
}

- (void)addToOverlayView:(UIView*)view {
    if (!overlayView) {
        CGRect frame = [self rectForOverlayView];
        overlayView = [[UIView alloc] initWithFrame:frame];
        overlayView.autoresizesSubviews = YES;
        overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        NSInteger htmlViewIndex = [htmlView.superview.subviews indexOfObject:htmlView];
        if (htmlViewIndex != NSNotFound) {
            [htmlView.superview addSubview:overlayView];
        }
    }
    
    view.frame = overlayView.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [overlayView addSubview:view];
}

- (void)resetOverlayView {
    if (overlayView && !overlayView.subviews.count) {
        [overlayView removeFromSuperview];
        //UCBREW_RELEASE (overlayView);
    }
}

- (NSString*) title
{
    if ([self.boxId isEqualToString: @"inbox"]) {
        return NSLocalizedString(@"Inbox", nil);
    } else if ([self.boxId isEqualToString: @"outbox"]) {
        return NSLocalizedString(@"Sent", nil);
    } else if ([self.boxId isEqualToString: @"deleted"]) {
        return NSLocalizedString(@"Deleted", nil);
    }
    
    return NSLocalizedString(@"Unknown", nil);
}

- (void) showLoading:(BOOL)show
{
    UCBREW_CALL_METHOD;
    if (show) {
        if (!self.model.isLoaded || ![self canShowModel]) {
            NSString* title = NSLocalizedString(@"Loading mail ...", nil);
            if (title.length) {
                TTActivityLabel* label = [[[TTActivityLabel alloc] initWithStyle:TTActivityLabelStyleWhiteBox]
                                          autorelease];
                label.text = title;
                label.backgroundColor = self.htmlView.backgroundColor;
                self.loadingView = label;
                [self addToOverlayView: self.loadingView];
            }
        }
        
    } else {
        [self.loadingView removeFromSuperview];
        [self resetOverlayView];
        self.loadingView = nil;
    }
}

- (void) createModel
{
    self.model = [USMailWebModel modelWithBoxId: self.boxId
                                         mailId: self.mailId];
}

- (void) modelDidFinishLoad:(id<TTModel>)model
{
    UCBREW_CALL_METHOD;
    USMailWebModel* mailModel = nil;
    mailModel = UCBREW_DYNAMIC_CAST(model, USMailWebModel);
    NSMutableString* mailHTMLString = [NSMutableString stringWithCapacity: 1024];
    if (!![mailModel.mailSubject length]) {
        [mailHTMLString appendFormat: @"<div class=\"subject\">%@</div>", mailModel.mailSubject];
    }
    [mailHTMLString appendString: @"<div class=\"article\">"];
    [mailHTMLString appendString: @"<div class=\"article_header\">"];
    [mailHTMLString appendString: @"<span class=\"author\">"];
    [mailHTMLString appendFormat: @"%@", mailModel.mailAuthor];
    [mailHTMLString appendString: @"</span>"];
    [mailHTMLString appendString: @"<span class=\"date\">"];
    [mailHTMLString appendString: mailModel.mailDate];
    [mailHTMLString appendString: @"</span>"];
    [mailHTMLString appendString: @"</div>"];
    [mailHTMLString appendString: @"<div class=\"line\"></div>"];
    [mailHTMLString appendString: @"<div class=\"content\">"];
    [mailHTMLString appendString: mailModel.mailBody];
    [mailHTMLString appendString: @"</div>"];
    [mailHTMLString appendString: @"</div>"];
    [self.htmlView loadHTMLBody: mailHTMLString baseUrl: @"http://m.newsmth.net"];
    [self invalidateView];
}

- (void) modelDidStartLoad:(id<TTModel>)model
{
    [super modelDidStartLoad: model];
    [self invalidateView];
    UCBREW_CALL_METHOD;
}

- (void) model:(id<TTModel>)model didFailLoadWithError:(NSError *)error
{
    [super model:model didFailLoadWithError: error];
    [self invalidateView];
    UCBREW_CALL_METHOD;
}

- (void) model:(id<TTModel>)model didUpdateObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    [super model: model didUpdateObject: object atIndexPath: indexPath];
    [self invalidateView];
    UCBREW_CALL_METHOD;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    UCBREW_CALL_METHOD;
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL* url = request.URL;
    if ([url.absoluteString isEqualToString: @"about:blank"]) {
        return YES;
    } else if ([url.scheme isEqualToString: @"ucsmth"]) {
        //USArticleWebModel* m = (USArticleWebModel*) [self model];
        //[m load: TTURLRequestCachePolicyNetwork more: YES];
        return NO;
    } else if ([url.host isEqualToString: @"newsmth.net"]){
        return NO;
    } else if ([url.host isEqualToString: @"m.newsmth.net"] && !![url.query length]){
        return YES;
    } else {
        //[USWebController presentSharedModelControllerWithURL: request.URL.absoluteString];
        return NO;
    }
    return NO;
}

- (void) reloadArticleModel
{
    [self invalidateModel];
    [self invalidateView];
}
@end

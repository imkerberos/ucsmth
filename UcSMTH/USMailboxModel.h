//
//  USMailboxModel.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USHtmlRequestModel.h"

@interface USMailboxModel : USHtmlRequestModel
@property (nonatomic, readonly) NSUInteger indexOfPage;
@property (nonatomic, readonly) NSUInteger totalOfPage;
@property (nonatomic, readonly) NSUInteger startIndexOfPage;
@property (nonatomic, readonly, copy) NSString* boxId;
@property (nonatomic, readonly, retain) NSMutableArray* mailFeeds;
+ (id) modelWithBoxId: (NSString*) aBoxId startIndexOfPage: (NSUInteger) aStartIndexOfPage;
@end

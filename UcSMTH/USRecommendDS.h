//
//  USRecommendDS.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-19.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTSectionedDataSource.h"

@interface USRecommendDS : TTSectionedDataSource
+ (id) dataSource;
@end

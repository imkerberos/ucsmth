//
//  USRootVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-7-3.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcSMTHEngine.h"
#import "USNavigationController.h"
#import "USBoardVC.h"
#import "USRecommendVC.h"
#import "USFavorVC.h"
#import "USForumLVC.h"
#import "USMailLVC.h"
#import "USSettingsVC.h"
#import "USFeedbackVC.h"
#import "USLoginVC.h"
#import "USExperimentVC.h"
#import "USArticleComposer.h"
#import "USRootVC.h"


#define kLogoutAlertViewTag 1000

@interface USRootVC () <UIAlertViewDelegate>

@end

@implementation USRootVC

+ (id) viewController
{
    USRootVC* vc = [[USRootVC alloc] initWithStyle:UITableViewStylePlain];
    return [vc autorelease];
}

+ (id) viewControllerWithStyle:(UITableViewStyle)style
{
    USRootVC* vc = [[USRootVC alloc] initWithStyle:style];
    return [vc autorelease];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    /* show user in left title */
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
     self.navigationItem.rightBarButtonItem = nil;
}

- (void) createRightNavButton
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    if (engine.isLogin) {
        self.navigationItem.rightBarButtonItem = nil;
        NSString* userId = [engine userId];
        if (![userId isEqualToString: @"guest"]) {
            UIBarButtonItem* barbutton_item = [[UIBarButtonItem alloc] initWithTitle: userId
                                                                               style: UIBarButtonItemStylePlain
                                                                              target: self
                                                                              action: @selector(onUserButtonClicked)];
            self.navigationItem.rightBarButtonItem = barbutton_item;
            [barbutton_item release];
        }
        UCBREW_LOG_OBJECT(userId);
    } else {
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"Login", nil)
                                                                                   style: UIBarButtonItemStyleBordered
                                                                                  target: self
                                                                                  action: @selector(onLoginButtonClicked)]
                                                                    autorelease];
    }

}
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    [self createRightNavButton];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString*) title
{
    return NSLocalizedString(@"UcSMTH", nil);
}


- (MwfTableData*) createAndInitTableData
{
    MwfTableData* table_data = [MwfTableData createTableData];
    [table_data addRow: @"recommend"];
    [table_data addRow: @"favorite"];
    [table_data addRow: @"forums"];
    [table_data addRow: @"mail"];
    [table_data addRow: @"settings"];
#ifdef DEBUG
    [table_data addRow: @"feedback"];
    [table_data addRow: @"expirement"];
    [table_data addRow: @"composer"];
#endif
    return table_data;
}

- $cellFor (NSString)
{
    static NSString* identifier = @"HomeMenuCell";
    UITableViewCell* cell =  [self.tableView dequeueReusableCellWithIdentifier: identifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                       reuseIdentifier: identifier]
                autorelease];
    }
    
    return cell;
}

-  $configCell (NSString, UITableViewCell)
{
    if ([item isEqualToString: @"recommend"]) {
        cell.textLabel.text = NSLocalizedString(@"Recommend", nil);
        cell.imageView.image = [UIImage imageNamed:@"recommend.png"];
    } else if ([item isEqualToString: @"favorite"]) {
        cell.textLabel.text = NSLocalizedString(@"Favorite", nil);
        cell.imageView.image = [UIImage imageNamed:@"favorite.png"];
    } else if ([item isEqualToString: @"forums"]) {
        cell.textLabel.text = NSLocalizedString(@"Forum", nil);
        cell.imageView.image = [UIImage imageNamed:@"forum.png"];
    } else if ([item isEqualToString: @"mail"]) {
        cell.textLabel.text = NSLocalizedString(@"Mail", nil);
        cell.imageView.image = [UIImage imageNamed:@"mail.png"];
    } else if ([item isEqualToString: @"settings"]) {
        cell.textLabel.text = NSLocalizedString(@"Settings", nil);
        cell.imageView.image = [UIImage imageNamed:@"settings.png"];
    } else if ([item isEqualToString: @"feedback"]) {
        cell.textLabel.text = NSLocalizedString(@"Feedback", nil);
    } else if ([item isEqualToString: @"expirement"]) {
        cell.textLabel.text = NSLocalizedString(@"Experiment", nil);
    } else if ([item isEqualToString:@"composer"]) {
        cell.textLabel.text = @"composer";
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTTableViewController* vc = nil;
    switch (indexPath.row) {
        case 0:
            vc = [USRecommendVC viewController];
            [self.navigationController pushViewController:vc animated: YES];
            break;
        case 1:
            [self.navigationController pushViewController: [USFavorVC viewController] animated: YES];
            break;
        case 2:
            [self.navigationController pushViewController: [USForumLVC viewController] animated: YES];
            break;
        case 3:
            [self.navigationController pushViewController: [USMailLVC viewController] animated: YES];
            break;
        case 4:
            [self.navigationController pushViewController: [USSettingsVC viewController] animated: YES];
            break;
        case 5:
            [self.navigationController pushViewController: [USFeedbackVC viewController] animated: YES];
            break;
        case 6:
            [self.navigationController pushViewController: [USExperimentVC viewController] animated: YES];
            break;
        case 7: {

        USArticleComposer* vc = nil;
        vc = [USArticleComposer composerWithMode: USArticleComposerModeNew];
        vc.boardId = @"Test";
        vc.articleId = @"";
        vc.delegate = nil;
        vc.subject = @"测试";
        vc.body = @"\n测试光标";
        [self.navigationController pushViewController: vc animated: YES];
        break;
        }
        default:
            break;
    }
}

- (void) onLoginButtonClicked
{
    [self presentLoginVCWithAnimated: YES];
}

- (void) onUserButtonClicked
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Logout", nil)
                                                        message: NSLocalizedString(@"Do you want to logout?", nil)
                                                       delegate: self
                                              cancelButtonTitle: NSLocalizedString(@"Cancel", nil)
                                              otherButtonTitles: NSLocalizedString(@"Logout", nil), nil];
    alert_view.tag = kLogoutAlertViewTag;
    [alert_view show];
    [alert_view release];
}

- (void) presentLoginVCWithAnimated:(BOOL)animated
{
    USLoginVC* vc = [USLoginVC viewControllerWithDelegate: self];
    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc]
                                   autorelease];
    [self presentModalViewController: nav animated: animated];
}

- (void) loadView
{
    [super loadView];
}

- (void) loginDidFinish: (USLoginVC *)controller
{
    [self dismissModalViewControllerAnimated: YES];
    self.navigationItem.rightBarButtonItem = nil;
}

- (void) loginDidCancel:(USLoginVC *)controller
{
    [self dismissModalViewControllerAnimated: YES];
}

- (void) alertView: (UIAlertView*) alertView didDismissWithButtonIndex: (NSInteger) buttonIndex
{
    if (alertView.tag == kLogoutAlertViewTag && buttonIndex == 1) {
        UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
        [engine userLogout];
        [self createRightNavButton];
    }
}
@end

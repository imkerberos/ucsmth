//
//  USSettingsVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "UcSMTHEngine.h"
#import "USSettingsVC.h"
#import "USViewModeSelectVC.h"
#import "SSKeychain.h"

@interface USSettingsVC () <USViewModeSelectVCDelegate>
@property (nonatomic, retain) UITextField* useranmeField;
@property (nonatomic, retain) UITextField* passwordField;
@property (nonatomic, retain) UISwitch* autologinSwitch;
@property (nonatomic, retain) UISwitch* showTopArticleSwitch;
@property (nonatomic, retain) UISwitch* showImageSwitch;
@property (nonatomic, retain) UISwitch* postRefreshSwitch;
@property (nonatomic, retain) UISwitch* postUseSignatureSwitch;
@property (nonatomic, assign) CGFloat fontSize;
@property (nonatomic, assign) NSUInteger viewMode;
@end

@implementation USSettingsVC
{
@private
    UISwitch* _postUseSignatureSwitch;
}

@synthesize useranmeField;
@synthesize passwordField;
@synthesize autologinSwitch;
@synthesize showTopArticleSwitch;
@synthesize showImageSwitch;
@synthesize postRefreshSwitch;
@synthesize fontSize;
@synthesize viewMode;
@synthesize postUseSignatureSwitch = _postUseSignatureSwitch;


+ (id) viewController
{
    USSettingsVC* vc = [[USSettingsVC alloc] initWithStyle: UITableViewStyleGrouped];
    return [vc autorelease];
}

- (id) initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle: style];
    if (self) {
        self.resizeWhenKeyboardPresented = YES;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView
{
    [super loadView];
    self.useranmeField = [[[UITextField alloc] initWithFrame: CGRectZero] autorelease];
    self.passwordField = [[[UITextField alloc] initWithFrame: CGRectZero] autorelease];
    self.autologinSwitch = [[[UISwitch alloc] initWithFrame:CGRectZero] autorelease];
    self.showTopArticleSwitch = [[[UISwitch alloc] initWithFrame: CGRectZero] autorelease];
    self.showImageSwitch = [[[UISwitch alloc] initWithFrame: CGRectZero] autorelease];
    self.postRefreshSwitch = [[[UISwitch alloc] initWithFrame: CGRectZero] autorelease];
    self.postUseSignatureSwitch = [[[UISwitch alloc] initWithFrame: CGRectZero] autorelease];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    self.autologinSwitch.on = [[engine userDefaults] boolForKey: @"autologin"];
    self.showImageSwitch.on = [[engine userDefaults] boolForKey: @"showimage"];
    self.postRefreshSwitch.on = [[engine userDefaults] boolForKey: @"postrefresh"];
    self.postUseSignatureSwitch.on = [[engine userDefaults] boolForKey: @"postsignature"];
    self.showTopArticleSwitch.on = [[engine userDefaults] boolForKey: @"showtoparticle"];
    self.useranmeField.text = [[engine userDefaults] stringForKey: @"username"];
    self.useranmeField.placeholder = NSLocalizedString(@"Please input username.", nil);
    self.useranmeField.keyboardType = UIKeyboardTypeAlphabet;
    self.passwordField.text = [SSKeychain passwordForService: [engine bundleIdentifier] account: self.useranmeField.text];
    self.passwordField.secureTextEntry = YES;
    self.passwordField.placeholder = NSLocalizedString(@"Please input password", nil);
    self.passwordField.keyboardType = UIKeyboardTypeAlphabet;
    self.fontSize = [[engine userDefaults] floatForKey: @"fontsize"];
    if (self.fontSize < 12) {
        self.fontSize = 16.0f;
    }
    self.viewMode = [[engine userDefaults] integerForKey: @"viewmode"];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.autologinSwitch = nil;
    self.showImageSwitch = nil;
    self.postRefreshSwitch = nil;
    self.showTopArticleSwitch = nil;
    self.useranmeField = nil;
    self.passwordField = nil;
}

- (void) dealloc
{
    UCBREW_RELEASE (autologinSwitch);
    UCBREW_RELEASE (showImageSwitch);
    UCBREW_RELEASE (postRefreshSwitch);
    UCBREW_RELEASE (showTopArticleSwitch);
    UCBREW_RELEASE (useranmeField);
    UCBREW_RELEASE (passwordField);
    [_postUseSignatureSwitch release], _postUseSignatureSwitch = nil;
    [super dealloc];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    [[engine userDefaults] setBool: self.autologinSwitch.on forKey: @"autologin"];
    [[engine userDefaults] setBool: self.showImageSwitch.on forKey: @"showimage"];
    [[engine userDefaults] setBool: self.postRefreshSwitch.on forKey: @"postrefresh"];
    [[engine userDefaults] setBool: self.postUseSignatureSwitch.on forKey: @"postsignature"];
    [[engine userDefaults] setBool: self.showTopArticleSwitch.on forKey: @"showtoparticle"];
    [[engine userDefaults] setFloat: self.fontSize forKey: @"fontsize"];
    [[engine userDefaults] setInteger: self.viewMode forKey: @"viewmode"];
    [[engine userDefaults] setObject: self.useranmeField.text forKey: @"username"];
    [SSKeychain setPassword: self.passwordField.text forService: [engine bundleIdentifier] account: self.useranmeField.text];
    [[engine userDefaults] synchronize];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString*) title
{
    return NSLocalizedString(@"Settings", nil);
}

- (NSString*) fontSizeLabel: (CGFloat) size
{
    if (size == 16.0f) {
        return NSLocalizedString(@"Middle", nil);
    } else if (size == 14.0f) {
        return NSLocalizedString(@"Small", nil);
    } else if (size == 20.0f) {
        return NSLocalizedString(@"Large", nil);
    } else {
        return NSLocalizedString(@"Invalid", nil);
    }
}

- (NSString*) viewModeLabel: (NSUInteger) mode
{
    if (mode == 0) {
        return NSLocalizedString(@"Classic", nil);
    } else if (mode == 1){
        return NSLocalizedString(@"Abstract", nil);
    } else if (mode == 2){
        return NSLocalizedString(@"Thread", nil);
    } else if (mode == 3){
        return NSLocalizedString(@"Saved", nil);
    } else {
        return NSLocalizedString(@"Invalid", nil);
    }
}

- (void) createModel
{
    self.dataSource = [TTSectionedDataSource dataSourceWithObjects:
#ifdef DEBUG
            NSLocalizedString(@"Account Managment", nil),
            [TTTableSettingsItem itemWithText: NSLocalizedString(@"Anonymous", nil)
                                      caption: NSLocalizedString(@"Account", nil)
                                    accessory: UITableViewCellAccessoryDisclosureIndicator],
#endif
            NSLocalizedString(@"Account", nil),
            [TTTableControlItem itemWithCaption: NSLocalizedString(@"Username", nil)
                                        control: self.useranmeField],
            [TTTableControlItem itemWithCaption: NSLocalizedString(@"Password", nil)
                                        control: self.passwordField],
            [TTTableControlItem itemWithCaption: NSLocalizedString(@"Autologin", nil)
                                        control: self.autologinSwitch],
            NSLocalizedString(@"Board", nil),
            [TTTableSettingsItem itemWithText: [self viewModeLabel: self.viewMode]
                                      caption: NSLocalizedString(@"View Mode", nil)
                                    accessory: UITableViewCellAccessoryDisclosureIndicator],
            [TTTableControlItem itemWithCaption: NSLocalizedString(@"Show top topic", nil)
                                        control: self.showTopArticleSwitch],
            NSLocalizedString(@"View", nil),
            [TTTableControlItem itemWithCaption: NSLocalizedString(@"Show Image", nil)
                                        control: self.showImageSwitch],
            NSLocalizedString(@"Post", nil),
            [TTTableControlItem itemWithCaption: NSLocalizedString(@"Refresh Article", nil)
                                        control: self.postRefreshSwitch],
            [TTTableControlItem itemWithCaption: NSLocalizedString(@"Use Signature", nil)
                                        control: self.postUseSignatureSwitch],
#ifdef DEBUG
            NSLocalizedString(@"Cache", nil),
            [TTTableTextItem itemWithText: NSLocalizedString(@"Clear Cache", nil)],
#endif
            nil];
}

- (void) didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0) {
        USViewModeSelectVC* vc = [USViewModeSelectVC viewControllerWithMode: self.viewMode delegate: self];
        [self.navigationController pushViewController: vc animated: YES];
    } else if (indexPath.section == 3 && indexPath.row == 0) {
        UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
        [engine invalidLogin];
    } else {
        [super didSelectObject: object atIndexPath: indexPath];
    }
}

- (void) viewModeSelectDidFinish:(USViewModeSelectVC *)viewController mode:(NSInteger)mode
{
    UCBREW_LOG(@"SELECT mode: %d", mode);
    self.viewMode = mode;
#if 0
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    [[engine userDefaults] setInteger: self.viewMode forKey: @"viewmode"];
#endif
    [self invalidateModel];
}
@end
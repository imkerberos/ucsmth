//
// Created by kerberos on 12-8-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "USTableMoreButton.h"
#import "USTableMoreButtonCell.h"

@implementation USTableMoreButton
{

}

- (Class) cellClass
{
    return [USTableMoreButtonCell class];
}

+ (id) itemWithText: (NSString*) text subtitle: (NSString*) subtitle
          accessory: (UITableViewCellAccessoryType) accessoryType
{
    USTableMoreButton* button = [USTableMoreButton itemWithText: text
                                                       subtitle: subtitle];
    button.accessoryType = accessoryType;
    return button;
}

+ (id) itemWithText: (NSString*) text subtitle: (NSString*) subtitle
        delegate: (id) delegate selector: (SEL) selector
          accessory: (UITableViewCellAccessoryType) accessoryType
{
    USTableMoreButton* button = [USTableMoreButton itemWithText: text delegate: delegate
                                                       selector: selector];
    button.subtitle = subtitle;
    button.accessoryType = accessoryType;
    return button;
}


@end
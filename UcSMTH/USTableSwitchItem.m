//
//  USTableSwitchItem.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USTableSwitchItemCell.h"
#import "USTableSwitchItem.h"

@implementation USTableSwitchItem
@synthesize text;
@synthesize switchView;

- (NSString*) reuseIdentifier
{
    return NSStringFromClass([USTableSwitchItemCell class]);
}

- (void) dealloc
{
    UCBREW_RELEASE(switchView);
    UCBREW_RELEASE(text);
    [super dealloc];
}
@end

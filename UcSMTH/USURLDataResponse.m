//
//  USURLDataResponse.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USURLDataResponse.h"

@implementation USURLDataResponse
+ (id) response
{
    USURLDataResponse* resp = [[USURLDataResponse alloc] init];
    return [resp autorelease];
}
@end

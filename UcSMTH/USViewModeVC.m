//
// Created by kerberos on 12-8-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "USViewModeVC.h"
#import "USViewModeSelectVC.h"

@interface USViewModeVC ()
@property (nonatomic, assign) NSUInteger mode;
@end

@implementation USViewModeVC
{
@private
    id <USViewModeVCDelegate> _delegate;
    NSUInteger _mode;
}
@synthesize delegate = _delegate;
@synthesize mode = _mode;

+ (id) viewControllerWithDelegate: (id) delegate
{
    return [USViewModeVC viewControllerWithMode: -1 delegate: delegate];
}

+ (id) viewControllerWithMode: (NSInteger) aMode delegate: (id <USViewModeVCDelegate>) delegate
{
    USViewModeVC* vc = [[USViewModeVC alloc] initWithStyle:UITableViewStylePlain mode: aMode];

    vc.delegate = delegate;
    return [vc autorelease];
}

- (id) initWithStyle:(UITableViewStyle)style mode: (NSInteger) aMode
{
    self = [super initWithStyle: style];
    if (self) {
        self.mode = aMode;
    }

    return self;
}

- (NSString*) title
{
    return NSLocalizedString(@"View Mode", nil);
}

- (void) createModel
{
    NSMutableArray* mode_array = [NSMutableArray arrayWithCapacity: 5];
    TTTableTextItem* item = nil;

    item = [TTTableTextItem itemWithText: NSLocalizedString(@"Thread", nil)];
    item.userInfo = [NSNumber numberWithInteger: 2];
    [mode_array addObject: item];

    item = [TTTableTextItem itemWithText: NSLocalizedString(@"Classic", nil)];
    item.userInfo = [NSNumber numberWithInteger: 0];
    [mode_array addObject: item];
#if 0
    item = [TTTableTextItem itemWithText: NSLocalizedString(@"Marked", nil)];
    item.userInfo = [NSNumber numberWithInteger: 1];
    [mode_array addObject: item];

    item = [TTTableTextItem itemWithText: NSLocalizedString(@"Saved", nil)];
    item.userInfo = [NSNumber numberWithInteger: 3];
    [mode_array addObject: item];
#endif
    item = [TTTableTextItem itemWithText: NSLocalizedString(@"Auto", nil)];
    item.userInfo = [NSNumber numberWithInteger: -1];
    [mode_array addObject: item];

    for (TTTableTextItem* i in mode_array) {
        if ([(NSNumber*)(i.userInfo) intValue] == self.mode) {
            i.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            i.accessoryType = UITableViewCellAccessoryNone;
        }
    }

    self.dataSource = [TTListDataSource dataSourceWithItems:
        mode_array
    ];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) didSelectObject: (id) object atIndexPath: (NSIndexPath*) indexPath
{
    TTTableTextItem* item = UCBREW_DYNAMIC_CAST(object, TTTableTextItem);
    if (item) {
        NSArray* mode_array = nil;
        mode_array = [(TTListDataSource*)self.dataSource items];
        self.mode = [(NSNumber*) item.userInfo integerValue];
        //[self createModel];
        if (self.delegate && [self.delegate respondsToSelector: @selector(viewModeDidFinishSelect:mode:)]){
            [self.delegate viewModeDidFinishSelect: self mode: self.mode];
        }
   }
}

- (void) handleCancelAction
{
    if (self.delegate && [self.delegate respondsToSelector: @selector(viewModeSelectDidCancel:)]){
        [self.delegate viewModeSelectDidCancel: self];
    }
    /*
    [self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex: 1]
                                           animated:YES];
                                           */
}
@end
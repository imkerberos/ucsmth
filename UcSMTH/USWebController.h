//
//  USWebController.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-24.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTWebController.h"

@interface USWebController : TTWebController
+ (void) presentSharedModelControllerWithURL:(NSString *)url;
@end
